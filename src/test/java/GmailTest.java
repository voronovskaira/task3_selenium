import model.Message;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import po.AlertPO;
import po.GmailHomePagePO;
import po.GmailLoginPO;
import util.JAXBHandler;
import util.PropertiesReader;

public class GmailTest {
    Message message  = JAXBHandler.getMessageModelFromXML(PropertiesReader.getInstance().getMessageXMLPath());
    WebDriver driver = new DriverManager().getDriver();
    final String invalidEmail = "347bVbsds d";

    @Test
    public void testGmail() {
        driver.get(PropertiesReader.getInstance().getGmailURL());
        GmailLoginPO gmailLoginPO = new GmailLoginPO(driver);
        gmailLoginPO.inputLoginAndClick("voronovskatest@gmail.com");
        gmailLoginPO.inputPasswordAndClick("mino1234");
        GmailHomePagePO gmailHomePagePO = new GmailHomePagePO(driver);
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(invalidEmail);
        gmailHomePagePO.writeASubjectOfMessage(message.getSubject());
        gmailHomePagePO.writeAMessage(message.getMessage());
        gmailHomePagePO.sendAMessage();

        AlertPO alertPO = new AlertPO(driver);
        Assert.assertTrue(alertPO.getAlertMessage().contains(invalidEmail));
        alertPO.clickOkOnAlertPopup();

        alertPO.clickCloseOnAlertPopup();
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo(message.getReceiver());
        gmailHomePagePO.writeASubjectOfMessage(message.getSubject());
        gmailHomePagePO.writeAMessage(message.getMessage());
        gmailHomePagePO.sendAMessage();

        gmailHomePagePO.clickOnSentMessagesButton();
        gmailHomePagePO.openLastMessage();
        Assert.assertTrue(gmailHomePagePO.getMessageDetails().contains(message.getMessage()));
        Assert.assertTrue(gmailHomePagePO.getMessageDetails().contains("voronovskatest@gmail.com"));
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}

