package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {
    private static Properties prop;
    private static PropertiesReader instance;

    private PropertiesReader() {
        prop = new Properties();
        loadConfigFile();
    }

    public static PropertiesReader getInstance() {
        if (instance != null)
            return instance;
        else
            return instance = new PropertiesReader();
    }

    private void loadConfigFile() {
        try (InputStream input = new FileInputStream("src/main/resources/config.properties")) {
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String getMessageXMLPath() {
        return prop.getProperty("messages_path");
    }

    public String getChromeDriverPath() {
        return prop.getProperty("chrome_driver_path");
    }

    public String getGmailURL() {
        return prop.getProperty("gmail_url");
    }


    public int getImplicityWait() {
        return new Integer(prop.getProperty("implicity_wait"));
    }


}
