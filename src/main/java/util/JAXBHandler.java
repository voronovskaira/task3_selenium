package util;

import model.Message;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class JAXBHandler {

    public static Message getMessageModelFromXML(String filepath) {
        try {
            Message message;
            JAXBContext context = JAXBContext.newInstance(Message.class);
            Unmarshaller um = context.createUnmarshaller();
            message = (Message) um.unmarshal(new File(filepath));
            return message;
        } catch (JAXBException e) {
            System.out.println(String.format("problem with xml file %s", filepath));
        }
        return null;
    }
}
