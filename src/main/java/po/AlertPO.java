package po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlertPO extends BasePage {
    WebDriverWait wait = new WebDriverWait(driver, 10);

    public AlertPO(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@role='alertdialog']/div[2]")
    WebElement alertMessageLine;

    @FindBy(xpath = "//button[@name='ok']")
    WebElement okButtonOnAlertPopup;

    @FindBy(xpath = "//img[@alt='Закрити']")
    WebElement closeButtonOnAlertPopup;

    public String getAlertMessage() {
        wait.until(ExpectedConditions.elementToBeClickable(alertMessageLine));
        return alertMessageLine.getText();
    }

    public void clickOkOnAlertPopup() {
        wait.until(ExpectedConditions.elementToBeClickable(okButtonOnAlertPopup));
        okButtonOnAlertPopup.click();
    }
    public void clickCloseOnAlertPopup(){
        wait.until(ExpectedConditions.elementToBeClickable(closeButtonOnAlertPopup));
        closeButtonOnAlertPopup.click();
    }
}
