package po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailLoginPO extends BasePage {
    @FindBy(name = "identifier")
    private WebElement loginInput;

    @FindBy(xpath = "//*[@id=\"identifierNext\"]/span/span")
    private WebElement submitLogin;

    @FindBy(name = "password")
    private WebElement passwordInput;

    @FindBy(id = "passwordNext")
    WebElement passwordNextButton;

    public GmailLoginPO(WebDriver driver) {
        super(driver);
    }

    public void inputLoginAndClick(String login) {
        this.loginInput.sendKeys(login);
        this.submitLogin.click();
    }

    public void inputPasswordAndClick(String password) {
        this.passwordInput.sendKeys(password);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("passwordNext")));
       // passwordNextButton.click();
        jsClick(passwordNextButton);
    }

}
