import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import util.PropertiesReader;

import java.util.concurrent.TimeUnit;

public class DriverManager {

    public WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", PropertiesReader.getInstance().getChromeDriverPath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(PropertiesReader.getInstance().getImplicityWait(), TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver;
    }
}
